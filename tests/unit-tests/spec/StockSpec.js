/*jslint browser: true, plusplus:true*/

var stockquotes = window.stockquotes;

describe("scenario", function () {

    it("verify init function", function () {
        expect(stockquotes.init()).toBe(undefined);
    });

    it("should create DOM elements for container and title", function () {
        expect(stockquotes.initHTML().querySelector("h1").innerText).toBe("Real time Stockquotes app");
    });

    it("stockquotes.series should not be empty", function () {
        expect(stockquotes.series).not.toBe({});
    });

    it("table should be 26 rows", function () {
        stockquotes.parseData(data.query.results.row);
        expect(stockquotes.showData().querySelectorAll("tr").length).toBe(26);
    });

    it("createValidCSSNameFromCompany should remove special characters from given string", function () {
        expect(stockquotes.createValidCSSNameFromCompany("HGF>&#$%6*")).toBe("HGF6");
    });

    it("parseData should fill stockquotes.series['BCS'][0] with data.query.results.row[0] from data.js", function () {
        stockquotes.parseData(data.query.results.row[0]);
        expect(JSON.stringify(stockquotes.series["BCS"][0])).toBe(
            JSON.stringify({
                col0: "BCS",
                col1: "18.29",
                col2: "03\/09\/2015",
                col3: "10:29am",
                col4: "0.87",
                col5: "N\/A",
                col6: "N\/A",
                col7: "N\/A",
                col8: "8900"
            })
        );
    });

    it("getJSONString should get JSON string from argument and call parseData to fill series with the string", function () {
        var e = {
            target: {
                responseText: '{ "query": { "results": { "row": [{"col0": "BCS","col1": "18.29", "col2": "03\/09\/2015", "col3": "10:29am", "col4": "0.87","col5": "N\/A", "col6": "N\/A", "col7": "N\/A", "col8": "8900"}]}} }'
            }
        };
        stockquotes.getJSONString(e);
        expect(JSON.stringify(stockquotes.series["BCS"][0])).toBe(
            JSON.stringify({
                col0: "BCS",
                col1: "18.29",
                col2: "03\/09\/2015",
                col3: "10:29am",
                col4: "0.87",
                col5: "N\/A",
                col6: "N\/A",
                col7: "N\/A",
                col8: "8900"
            })
        );
    });


    it("rnd should return a number between given numbers", function () {
        var result = stockquotes.rnd(0, 10);
        expect(result).not.toBeLessThan(0);
        expect(result).not.toBeGreaterThan(11);
    });

    it("getFormattedDate function should return a string with mm/dd/yyyy date format", function () {
        var date = new Date("03/30/2015");
        expect(stockquotes.getFormattedDate(date)).toBe("03/30/2015");
    });

    it("getFormattedTime function should return a string with hh:mm am/pm format", function () {
        var date = new Date(2015, 3, 3, 17, 7, 38, 0);
        expect(stockquotes.getFormattedTime(date)).toBe("5:07 pm");
    });

    it("generateTestData function should fill series.BCS", function () {
        var oldData = stockquotes.series["BCS"].length, newData;
        stockquotes.generateTestData();
        newData = stockquotes.series["BCS"].length;
        expect(newData).toBeGreaterThan(oldData);
    });

    it("loop should be undefined", function () {
        expect(stockquotes.loop()).toBe(undefined);
    });



});