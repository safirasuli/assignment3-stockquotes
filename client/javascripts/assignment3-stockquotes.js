var stockquotes = window.stockquotes, io, data;

(function () {
    "use strict";

    window.stockquotes = {
        socket: io("http://server3.tezzt.nl:1337"),

        settings: {
            refresh: 1000,
            ajaxUrl: 'http://server3.tezzt.nl/~theotheu/stockquotes/index.php'
        },
        series: {},

        //Wacht op data van response
        retrieveData: function (e) {
            var str = e.target.responseText;
            var obj = JSON.parse(str);
            var rows = obj.query.results.row;
            stockquotes.parseData(rows);
        },

        getDataFromAjax: function () {
            var xhr;
            xhr = new XMLHttpRequest();
            xhr.open("GET", stockquotes.settings.ajaxUrl, true);
            xhr.addEventListener("load", stockquotes.retrieveData);
            xhr.send();
        },

        getRealTimeData: function () {
            stockquotes.socket.on("stockquotes", function (data) {
                stockquotes.parseData(data.query.results.row);
                var container;
                container = document.querySelector("#container");
                document.querySelector("#container").removeChild(document.querySelector("table"));

                container.appendChild(stockquotes.showData());
            });
        },

        // removes special characters from company name
        createValidCSSNameFromCompany: function (str) {
            return str.replace(/[^a-zA-Z_0-9]/g, "");
        },

        // returns a random float between min and max of current value
        rnd: function (min, max) {
            var returnvalue;
            returnvalue = Math.random() * (max - min + 1) + min;
            returnvalue = Math.round(returnvalue * 100) / 100;
            return returnvalue;
        },

        loop: function () {
            var container;
            //Dit is voor de testdata genereren
            //stockquotes.generateTestData();
            //Dit gaat met ajax ophalen
            stockquotes.getDataFromAjax();
            container = document.querySelector("#container");
            document.querySelector("#container").removeChild(document.querySelector("table"));

            container.appendChild(stockquotes.showData());

            setTimeout(stockquotes.loop, stockquotes.settings.refresh);
        },

        // return formatted time am, pm
        getFormattedTime: function (date) {
            var time, hours, minutes;
            hours = date.getHours().toFixed();
            minutes = date.getMinutes().toString();
            if (hours.length > 1) {
                if (minutes.length < 2) {
                    minutes = "0" + minutes.toString();
                }
                time = (24 - hours).toString() + ":" + minutes + " pm";
            } else {
                time = hours.toString() + ":" + minutes + " am";
            }

            return time;
        },

        // return formatted date string mm/dd/yyyy
        getFormattedDate: function (date) {
            var datetime, month, day, year;
            month = date.getMonth().toString();
            day = date.getDay().toString();
            year = date.getFullYear().toString();
            if (month.length < 2) {
                month = "0" + month;
            }
            if (day.length < 2) {
                day = "0" + day;
            }
            datetime = month + "/" + day + "/" + year;

            return datetime;
        },


        // generates random data
        generateTestData: function () {
            var company, quote, newQuote, date;
            date = new Date();
            for (company in stockquotes.series) {
                quote = stockquotes.series[company][0];
                newQuote = Object.create(quote);
                newQuote.col0 = company;
                newQuote.col2 = stockquotes.getFormattedDate(date);
                newQuote.col3 = stockquotes.getFormattedTime(date);
                newQuote.col4 = stockquotes.rnd(-10, 10); //price difference between current and last value
                stockquotes.series[company].push(newQuote);
            }
        },

        //Parse data to use it. Rows would be the object we Parse.
        parseData: function (rows) {
            var i, company;
            for (i = 0; i < rows.length; i += 1) {
                company = rows[i].col0;
                if (stockquotes.series[company] !== undefined) {
                    stockquotes.series[company].push(rows[i]);
                } else {
                    stockquotes.series[company] = [rows[i]];
                }
            }
        },

        //ChangeColor depending on rising or decending of prices
        changeColor: function (col, row) {
            if (col > 0) {
                row.className = "stijgen";
            } else if (col < 0) {
                row.className = "dalen";
            } else {
                row.className = "neutraal";
            }
        },

        //show data in table
        showData: function () {
            var tableNode, tr, col, attr, quote, company, header, row, cell;

            tableNode = document.createElement("table");
            //create header
            header = tableNode.createTHead();
            row = header.insertRow(0);
            cell = row.insertCell(0);
            cell.innerHTML = "Company";
            cell = row.insertCell(1);
            cell.innerHTML = "  Date";
            cell = row.insertCell(2);
            cell.innerHTML = "  Time";
            cell = row.insertCell(3);
            cell.innerHTML = "  Price";

            for (company in stockquotes.series) {
                quote = stockquotes.series[company][stockquotes.series[company].length - 1];
                tr = document.createElement("tr");
                stockquotes.changeColor(quote.col4, tr);

                for (attr in quote) {
                    col = document.createElement("td");
                    col.innerText = quote[attr];
                    tr.appendChild(col);
                }
                tableNode.appendChild(tr);
            }

            return tableNode;
        },
        //add container and title
        initHTML: function () {
            var container, h1Node;

            container = document.createElement("div");
            container.id = "container";

            stockquotes.container = container;

            h1Node = document.createElement("h1");
            h1Node.innerText = "Real Time Stockquote App";
            stockquotes.container.appendChild(h1Node);
            return stockquotes.container;
        },

        //initializes the data and calls the functions
        init: function () {
            var container, bodyNode;
            bodyNode = document.querySelector("body");
            container = stockquotes.initHTML();
            bodyNode.appendChild(container);
            container.appendChild(stockquotes.showData());

            //use this if you want to get data from Ajax
           // app.getDataFromAjax();

            //Use this for use of Sockets
            stockquotes.getRealTimeData();

        }
    };
}());


