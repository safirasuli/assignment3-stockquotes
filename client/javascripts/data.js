/**
 * Created by Gebruiker on 13-1-2016.
 */
var data = {
    "query": {
        "count": 27,
        "created": "2015-03-09T10:29:04+01:00",
        "lang": "en-US",
        "diagnostics": {
            "publiclyCallable": "true",
            "url": {
                "execution-start-time": "1",
                "execution-stop-time": "67",
                "execution-time": "66",
                "content": "http:\/\/download.finance.yahoo.com\/d\/quotes.csv?s=ibm+orcl+bcs+stt+jpm+lgen.l+ubs+db+ben+cs+bk+kn.pa+gs+lm+ms+mtu+ntrs+gle.pa+bac+av+sdr.l+dodgx+slf+sl.l+nmr+ing+bnp.pa&f=sl1d1t1c1ohgv&e\u200c?=.csv"
            },
            "user-time": "68",
            "service-time": "66",
            "build-version": "0.2.1867"
        },
        "results": {
            "row": [
                {
                    "col0": "BCS",
                    "col1": "18.29",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "0.87",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "8900"
                },
                {
                    "col0": "STT",
                    "col1": "53.36",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-13.34",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "JPM",
                    "col1": "49.88",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-2.08",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "500"
                },
                {
                    "col0": "LGEN.L",
                    "col1": "212.11",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "15.71",
                    "col5": "196.40",
                    "col6": "197.90",
                    "col7": "195.20",
                    "col8": "2986239"
                },
                {
                    "col0": "UBS",
                    "col1": "19.75",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-1.04",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "DB",
                    "col1": "39.83",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-7.03",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "12700"
                },
                {
                    "col0": "BEN",
                    "col1": "59.49",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "8.64",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "CS",
                    "col1": "26.61",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-4.70",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "500"
                },
                {
                    "col0": "BK",
                    "col1": "35.36",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "4.61",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "KN.PA",
                    "col1": "3.45",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-0.30",
                    "col5": "3.66",
                    "col6": "3.769",
                    "col7": "3.654",
                    "col8": "3447906"
                },
                {
                    "col0": "GS",
                    "col1": "186.03",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "27.03",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "200"
                },
                {
                    "col0": "LM",
                    "col1": "28.39",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-5.41",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "MS",
                    "col1": "24.97",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-2.17",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "MTU",
                    "col1": "7.26",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "0.89",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "NTRS",
                    "col1": "64.03",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "9.30",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "GLE.PA",
                    "col1": "36.88",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-1.94",
                    "col5": "37.685",
                    "col6": "38.985",
                    "col7": "37.565",
                    "col8": "3261198"
                },
                {
                    "col0": "BAC",
                    "col1": "14.87",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "0.97",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "24750"
                },
                {
                    "col0": "AV",
                    "col1": "13.39",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "0.13",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "SDR.L",
                    "col1": "2454.34",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-156.66",
                    "col5": "2600.00",
                    "col6": "2612.00",
                    "col7": "2560.00",
                    "col8": "44957"
                },
                {
                    "col0": "DODGX",
                    "col1": "136.03",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-16.81",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "N\/A"
                },
                {
                    "col0": "SLF",
                    "col1": "35.88",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "3.84",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "SL.L",
                    "col1": "362.57",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "17.27",
                    "col5": "345.30",
                    "col6": "346.20",
                    "col7": "340.40",
                    "col8": "897480"
                },
                {
                    "col0": "NMR",
                    "col1": "6.69",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-1.09",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "ING",
                    "col1": "10.71",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-1.06",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "4000"
                },
                {
                    "col0": "BNP.PA",
                    "col1": "42.95",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "0",
                    "col5": "50.61",
                    "col6": "51.28",
                    "col7": "50.40",
                    "col8": "1576061"
                }
            ]
        }
    },
    "meta": {
        "description": "Stockquote simulator for CRIA-WT. Note that change is relative to initial value, not the previous value.",
        "parameters": ["cbfunc=<string> To force the json to be in a function. Default not set"],
        "sleep": ["sleep=<int> Program wil wait t seconds before returning. Objective is to simulate timeouts. Default set to 0"]
    }
};